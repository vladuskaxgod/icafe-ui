@echo off
reg add "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Edge\Recommended" /v DownloadRestrictions /t REG_DWORD /d 3 /f
reg add "HKEY_CURRENT_USER\Software\Policies\Google\Chrome" /v DownloadRestrictions /t REG_DWORD /d 3 /f